### Запуск для разработки(Windows)
```
setx DOCKER_TAG '$DOCKER_TAG'
setx HOST $(hostname)
docker-compose -f docker-stack.yml -f docker-overlay-dev.yml up
```

### Запуск фронта для разработки
```
cd web
yarn start
```

### Создание кэша
```
docker-compose -f docker-stack.yml -f docker-overlay-dev.yml exec app ./manage.py createcachetable
```

### Создание миграций
```
docker-compose -f docker-stack.yml -f docker-overlay-dev.yml exec app ./manage.py makemigrations
```

### Применение миграций
```
docker-compose -f docker-stack.yml -f docker-overlay-dev.yml exec app ./manage.py migrate
```