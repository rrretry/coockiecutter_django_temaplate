var jq = jQuery.noConflict();
(function ($) {
  $(document).on('change', '.filter', function () {
    common_id = $(this).val();
    if (common_id) {
      $('.filtered option').hide();
      $('.filtered option[common_id=' + common_id + ']').show();
    } else {
      $('.filtered option').show();
    }
  });
})(jq);
