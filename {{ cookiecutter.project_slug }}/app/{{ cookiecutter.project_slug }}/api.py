# coding: utf-8
from django.contrib.auth.models import User
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers, mixins, permissions, authentication
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response
from rest_framework.routers import SimpleRouter
from django.contrib.auth.models import AnonymousUser
from django.conf import settings
from common.models import ProjectInnerParticipant, ProjectRole

VERSION = getattr(settings, 'VERSION', 'dev')


class ProjectRoleSerializer(serializers.ModelSerializer):
    code = serializers.CharField(read_only=True, source='role')
    name = serializers.ReadOnlyField(source='role_name')
    project_id = serializers.IntegerField(read_only=True, source='project_general.project.id')


    class Meta:
        model = ProjectRole
        fields = ['project_id', 'code',  'name']
        ref_name = 'user_role'


class ProfileParticipantWithRoleSerializer(serializers.ModelSerializer):
    role = ProjectRoleSerializer(many=True, read_only=True, source='user.roles')

    class Meta:
        model = ProjectInnerParticipant
        exclude = ['user']
        depth = 1


class ProfileParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectInnerParticipant
        exclude = ['user']
        depth = 1


class UserWithParticipantSerializer(serializers.ModelSerializer):
    logged_in = serializers.SerializerMethodField()
    participant = ProfileParticipantWithRoleSerializer(read_only=True, source='inner_participant')

    def get_logged_in(self, obj):
        return not isinstance(obj, AnonymousUser)

    class Meta:
        model = User
        fields = (
            'id', 'username', 'first_name', 'last_name', 'date_joined', 'last_login', 'email', 'logged_in',
            'is_superuser', 'is_staff', 'participant',
        )


class ProfileSerializer(serializers.ModelSerializer):
    logged_in = serializers.SerializerMethodField()
    participant = ProfileParticipantSerializer(read_only=True, source='inner_participant')

    def get_logged_in(self, obj):
        return not isinstance(obj, AnonymousUser)

    class Meta:
        model = User
        fields = (
            'id', 'username', 'first_name', 'last_name', 'date_joined', 'last_login', 'email', 'logged_in',
            'is_superuser', 'is_staff', 'participant'
        )


class ProfileView(APIView):
    serializer_class = UserWithParticipantSerializer
    queryset = User.objects.all().select_related("permissions")
    permission_classes = (permissions.IsAuthenticated,)

    @swagger_auto_schema(operation_id='profile',
                         responses={200: openapi.Response('Profile user', UserWithParticipantSerializer)})
    def get(self, request):
        serializer = UserWithParticipantSerializer(request.user)
        return Response(serializer.data)


class VersionViewSet(mixins.ListModelMixin, GenericViewSet):
    permission_classes = ()

    def list(self, request, *args, **kwargs):
        return Response({'version': VERSION})


router = SimpleRouter()
router.register(r'api/version', VersionViewSet, base_name='version')
urlpatterns = router.urls
